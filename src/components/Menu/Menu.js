import React from 'react';
import './Menu.css'

const Menu = props => {
    return (
        <div className="Menu" onClick={props.addOrder}>
            <img src={props.image} alt="fastfood"/>
            <p>{props.name}</p>
            <p>Price: {props.price} KGS</p>
        </div>
    );
};

export default Menu;