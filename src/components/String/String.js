import React from 'react';
import './String.css'

const String = props => {
    return (
        <div className="String">
            <p>{props.name}</p>
            <p>x {props.count}</p>
            <p>{props.price} KGS</p>
            <button type="button" onClick={props.deleteOrder}><b>X</b></button>
        </div>
    );
};

export default String;