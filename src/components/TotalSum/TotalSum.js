import React from 'react';

const TotalSum = props => {
    let sum = 0;
    let totalSum = [0];

    props.order.forEach((item, ind) => {
        totalSum.push(item.price * item.count)
    });

    for (let i = 0; i < totalSum.length; i ++){
        sum = sum + parseInt(totalSum[i]);
    };

    if (sum === 0) {
        return (
            <p>Order is empty! Add something from menu.</p>
        )
    };

    return (
        <h4>
            Total price: {sum} KGS.
        </h4>
    );
};

export default TotalSum;