import React, {useState} from 'react';
import './App.css';
import Menu from "./components/Menu/Menu";
import hamImage from './assets/ccb65c065cb1ec7085bce4bcd8d676fb.jpg';
import cheeseImage from './assets/011d1d429a11a56939f6b4c2831f1061.jpg';
import friImage from './assets/12-126041_french-fries-vector-png-clipart.jpeg';
import coffImage from './assets/wall-murals-be-amazing-today-but-first-coffee-cartoon-vector-illustration-doodle-style.jpg.jpg';
import teaImage from './assets/index.jpeg';
import colaImage from './assets/6712033_preview.jpeg';
import TotalSum from "./components/TotalSum/TotalSum";
import String from "./components/String/String";

const MENU = [
  {name: 'Hamburger', price: 80, image: hamImage},
  {name: 'Cheeseburger', price: 90, image: cheeseImage},
  {name: 'Fries', price: 50, image: friImage},
  {name: 'Coffee', price: 70, image: coffImage},
  {name: 'Tea', price: 50, image: teaImage},
  {name: 'Coca-Cola', price: 40, image: colaImage},
];

function App() {
  const [order, setOrder] = useState([
    {name: 'Hamburger', count: 0, price: 80},
    {name: 'Cheeseburger', count: 0, price: 90},
    {name: 'Fries', count: 0, price: 50},
    {name: 'Coffee', count: 0, price: 70},
    {name: 'Tea', count: 0, price: 50},
    {name: 'Coca-Cola', count: 0, price: 40},
  ]);

  const addOrder = ind => {
    const copyOrder = [...order];
    let orderItem = {...copyOrder[ind]};
    orderItem.count++;
    copyOrder[ind] = orderItem;
    setOrder(copyOrder);
  };

  const deleteOrder = ind => {
    const copyOrder = [...order];
    let orderItem = {...copyOrder[ind]};
    if (orderItem.count > 0) {
      orderItem.count--;
      copyOrder[ind] = orderItem;
      setOrder(copyOrder);
    }
  };

  const orderArr = [];

  order.map((item, index) => {
      if (item.count === 0) {
        return (
            <div></div>
        )
    };
    return orderArr.push(
        <String
            key={index}
            name={item.name}
            count={item.count}
            price={item.price * item.count}
            deleteOrder={() => deleteOrder(index)}
        />
    );
  });

    const menu = MENU.map((item, index) => {
    return <Menu
        key={index}
        image={item.image}
        name={item.name}
        price={item.price}
        addOrder={() => addOrder(index)}
      />
  });

  return (
    <div className="App">
      <div className="menu-container">
        {menu}
      </div>

      <div>
        {orderArr}
      </div>

      <TotalSum
          order={order}
      />
    </div>
  );
}

export default App;
